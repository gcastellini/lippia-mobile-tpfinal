package com.crowdar.examples.constants;

public class TimeEntryConstants {
    public static final  String ADD_ENTRY_HEADER = "ACCESSIBILITY_ID:Add entry";

    public static final  String DAY_PICKER = "ACCESSIBILITY_ID:%s";

    public static final String ADD_ENTRY_LAYOUT = "id:constraintLayout";

    public static final String START_HOUR="xpath://*[@class='android.widget.Button' and @index='0']";

    public static final String START_MINUTE="xpath://android.widget.NumberPicker[@index='2']/android.widget.Button[@index='0']";
    public static final String START_MINUTE_2="xpath://android.widget.NumberPicker[@index='2']/android.widget.Button[@index='2']";
    public static final String END_SECTION="ACCESSIBILITY_ID:END";

    public static final String SAVE_BUTTON="id:floating_action_button";

    public static final String TIME_ENTRY="id:time_entry_duration";

    public static final String TIME_ENTRY_OPTIONS="ACCESSIBILITY_ID:Button more on time entry";

    public static final String DELETE="id:bottom_sheet_delete";

    public static final String DISCARD="ACCESSIBILITY_ID:Toolbar discard";
    public static final String DISCARD_2="id:discard_button";

    public static final String BACK="ACCESSIBILITY_ID:Navegar hacia arriba";
    public static final String NO_ENTRIES="id:empty_state_time_entries";

    //CONSTANTS SETTINGS
    public static final String SIDE_PANEL="ACCESSIBILITY_ID:Abrir panel lateral de navegación";

    public static final String SETTINGS="xpath://*[@text='Settings']";

    public static final String DARK_MODE="id:settings_dark_mode";

    public static final String LOG_OUT="xpath://*[@text='Log out']";

    public static final String CONFIRM="id:confirmation_button";

}
