package com.crowdar.examples.services;

import com.crowdar.core.actions.MobileActionManager;
import com.crowdar.examples.constants.HomeConstants;
import com.crowdar.examples.constants.TimeEntryConstants;
import org.joda.time.DateTime;
import org.testng.Assert;

import java.sql.Time;
import java.time.LocalDateTime;

import static java.lang.Math.abs;


public class TimeEntryService {

    public static void isViewLoaded() {
        MobileActionManager.waitVisibility(TimeEntryConstants.ADD_ENTRY_HEADER);
        Assert.assertTrue(MobileActionManager.isVisible(TimeEntryConstants.ADD_ENTRY_HEADER), HomeConstants.VIEW_NOT_DISPLAYED_MESSAGE);
    }

    public static void startEndSection() {
        MobileActionManager.click(TimeEntryConstants.ADD_ENTRY_LAYOUT);
    }

    public static void pickDay(String day) {
        MobileActionManager.click(TimeEntryConstants.DAY_PICKER, day);
    }

    public static void hourEntryStart(String hour, String minute) {
        DateTime time = new DateTime();
        int h = time.getHourOfDay();
        int m = LocalDateTime.now().getMinute();
        int clicksMinute;
        int clicksHour = abs(h - Integer.parseInt(hour));
        if (m > Integer.parseInt(minute)) {
            clicksMinute = abs(m - Integer.parseInt(minute));
            for (int i = 0; i < clicksMinute; i++) {
                MobileActionManager.click(TimeEntryConstants.START_MINUTE);
            }
        } else if (m < Integer.parseInt(minute)) {
            clicksMinute = abs(Integer.parseInt(minute) - m);
            for (int i = 0; i < clicksMinute; i++) {
                MobileActionManager.click(TimeEntryConstants.START_MINUTE_2);
            }
        }

        for (int i = 0; i < clicksHour; i++) {
            MobileActionManager.click(TimeEntryConstants.START_HOUR);
        }

    }

    public static void hourEntryEnd(String hour, String minute) {
        MobileActionManager.click(TimeEntryConstants.END_SECTION);
        DateTime time = new DateTime();
        int h = time.getHourOfDay();
        int m = LocalDateTime.now().getMinute();
        int clicksMinute;
        int clicksHour = abs(h - Integer.parseInt(hour));
        if (m > Integer.parseInt(minute)) {
            clicksMinute = abs(m - Integer.parseInt(minute));
            for (int i = 0; i < clicksMinute; i++) {
                MobileActionManager.click(TimeEntryConstants.START_MINUTE);
            }
        } else if (m < Integer.parseInt(minute)) {
            clicksMinute = abs(Integer.parseInt(minute) - m);
            for (int i = 0; i < clicksMinute; i++) {
                MobileActionManager.click(TimeEntryConstants.START_MINUTE_2);
            }
        }

        for (int i = 0; i < clicksHour; i++) {
            MobileActionManager.click(TimeEntryConstants.START_HOUR);
        }
        MobileActionManager.click(TimeEntryConstants.SAVE_BUTTON);

    }

    public static void saveButton() {
        MobileActionManager.click(TimeEntryConstants.SAVE_BUTTON);
    }

    public static void time() {
        MobileActionManager.waitVisibility(TimeEntryConstants.TIME_ENTRY);
    }

    public static void cleanSpace() {
        MobileActionManager.click(TimeEntryConstants.TIME_ENTRY_OPTIONS);
        MobileActionManager.click(TimeEntryConstants.DELETE);
        MobileActionManager.waitInvisibility(TimeEntryConstants.TIME_ENTRY);
    }

    public static void discard() {
        MobileActionManager.click(TimeEntryConstants.DISCARD);
        MobileActionManager.click(TimeEntryConstants.DISCARD_2);
        MobileActionManager.click(TimeEntryConstants.BACK);
        MobileActionManager.click(TimeEntryConstants.DISCARD_2);
    }

    public static void noEntries() {

        MobileActionManager.waitVisibility(TimeEntryConstants.NO_ENTRIES);
    }

    public static void clickSidePanel() {
        MobileActionManager.click(TimeEntryConstants.SIDE_PANEL);
    }

    public static void clickSettings() {

        MobileActionManager.click(TimeEntryConstants.SETTINGS);
    }

    public static void clickLogout() {
        MobileActionManager.click(TimeEntryConstants.LOG_OUT);
        MobileActionManager.click(TimeEntryConstants.CONFIRM);
    }

    public static void enableDarkMode() throws InterruptedException {
        MobileActionManager.click(TimeEntryConstants.DARK_MODE);
        Thread.sleep(2000);
        MobileActionManager.click(TimeEntryConstants.DARK_MODE);

    }

}
