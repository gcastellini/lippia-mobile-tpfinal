Feature: As a potential client i want to interact with the mobile application

  Background:
    Given The app is loaded correctly
    And Login page is displayed

    @login
  Scenario Outline: The user tries to log in
    When The user enters email <email>
    And The user enters password <password>
    And The user clicks on Log in button
    Then Home page is displayed
      #@Demo
    Examples:
      | email                           | password |
      | giuliana_castellini@hotmail.com | hola1234 |

  @addEntry
  Scenario Outline: The user adds time entry
    When The user logs in the application with: <email>, <password>
    And The user clicks on add button
    And Time Entry page is displayed
    And The user clicks on Start End Layout
    And The user sets start hour time <startHour>, <startMinute>
    And The user sets end hour time <endHour>, <endMinute>
    And The user clicks on save button
    Then A time entry is displayed

    #@Demo
    Examples:
      | email                           | password | startHour | startMinute | endHour | endMinute |
      | giuliana_castellini@hotmail.com | hola1234 | 10        | 20          | 12      | 20        |

@addDay
  Scenario Outline: The user adds time entry with date
    When The user logs in the application with: <email>, <password>
    And The user clicks on add button
    And Time Entry page is displayed
    And The user clicks on Start End Layout
    And The user selects day <day>
    And The user sets start hour time <startHour>, <startMinute>
    And The user sets end hour time <endHour>, <endMinute>
    And The user clicks on save button
    Then A time entry is displayed
  #@Demo
    Examples:
      | email                           | password | startHour | startMinute | endHour | endMinute | day             |
      | giuliana_castellini@hotmail.com | hola1234 | 10        | 20          | 12      | 20        | 18 noviembre 2022 |

  @notSave
  Scenario Outline: The user does not save time entry
    When The user logs in the application with: <email>, <password>
    And The user clicks on add button
    And Time Entry page is displayed
    And The user clicks on Start End Layout
    And The user sets start hour time <startHour>, <startMinute>
    And The user sets end hour time <endHour>, <endMinute>
    And The user discards time entry
    Then No entries are displayed
    #@Demo
    Examples:
      | email                           | password | startHour | startMinute | endHour | endMinute |
      | giuliana_castellini@hotmail.com | hola1234 | 10        | 20          | 12      | 20        |

  @Settings
  Scenario Outline: The user enables dark mode
    When The user logs in the application with: <email>, <password>
    And The user clicks on Side Panel
    And The user clicks on Settings
    Then The user enables dark mode
    #@Demo
    Examples:
      | email                           | password |
      | giuliana_castellini@hotmail.com | hola1234 |

  @logout
  Scenario Outline: The user performs log out
    When The user logs in the application with: <email>, <password>
    And The user clicks on Side Panel
    And The user performs log out
    Then Login page is displayed
    #@Demo
    Examples:
      | email                           | password |
      | giuliana_castellini@hotmail.com | hola1234 |